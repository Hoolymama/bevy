import bevy

def initializePlugin(mobject):
	bevy.load()

def uninitializePlugin(mobject):
	bevy.unload()
